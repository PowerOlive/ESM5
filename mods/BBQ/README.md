BBQ v1.11 by Grizzly Adam

https://forum.minetest.net/viewtopic.php?f=9&t=19324

https://github.com/Grizzly-Adam/BBQ

Goals
-----
-Expanding the cooking & food options in Minetest
-Add working smoker and grill to Minetest
-Utilize under-used items, such as vessels, vessel shelf, and mushrooms
-Be compatible with and expand on Mobs-Redo and it's Animals pack
-Be compatible with Food Rubenwardy's Food Mod
-Be compatible with Auke Kok's Crops Mod
-Be compatible with Farming-Redo Mod 

Documentation
-------------

For more information about what is in this pack, check the file: description.txt

Credits
---------

Created by Grizzly Adam

Feel free to resue parts or all of my work.

Exceptions:

* Potato image, Tomato image and original corn image taken from Auke Kok's Crops Mod
* Beef designs taken from Daniel_Smith, some were further modified
* All other textures created by Grizzly Adam

Have I forgotten to credit your work? Please tell me.

New In This Version
-------------------
Adjusted Kettle Grill fire animation
Fixed Kettle Grill crafting recipe.

New In Version 1.1
-------------------
New Items:
	Kettle Gril
	Lump Charcoal (fuel)
	Charcoal Briquettes (fuel)
	Bag O' Charcoal (fuel)
	Propane (fuel)
	Sawdust
	Foil

	Paprika
	Molasses (byproduct of Sugar)
	Steak sauce
	Vinegar
	Vinegar Mother

	Beer
	Cheese Steak
	Pizza
	Grilled Tomato
	Brisket
	Corned Beef
	Veggie Kebabs
	Lamb Kebabs
	Smoked Pepper
	Grilled Corn
	Stuffed Mushroom
	Portabella Steaks
	Pickled peppers
	Veggie Packets
	Corn
	Potato


New In Version 1.02
-------------------
Bug Fix: All Colours of Sheep Now Drop Mutton

Added support for Farming Redo
Improved support for Crops

New Items:
	Wood Pile (can store trees, wood slabs, and sticks)
	Smoker Blue Print (wall hanging)
	Yeast (Can be found when harvesting grass and jungle grass)
	Bacon Cheeseburger
	

Updated Textures:
	Raw Mutton
	Cooked Mutton
	Raw Leg of Lamb
	Cooked Leg of Lamb
	Sugar
	Brine
	Hotdog
	Hamburger
